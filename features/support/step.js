const { Given, When, Then, Before, After } = require('@cucumber/cucumber')
const { expect } = require('chai')
const fs = require('fs');
const puppeteer = require('puppeteer')
const request = require("request-promise");
// const request = require("request-promise-native");

const cheerio = require("cheerio");
const cicGA="?utm_source=autotest&utm_medium=CIC"


Before({timeout: 24 * 5000},async function () {
    this.browser = await puppeteer.launch({ 
        executablePath:
        "C:\\Program Files\\Google\\Chrome\\Application\\chrome.exe",
        // MAC路徑:
        // executablePath:
        // '/Applications/Google Chrome.app/Contents/MacOS/Google Chrome',
        headless:true,//有無需要開視窗,false要開,true不開
        slowMo:100,// slow down by 100ms
        devtools:false//有無需要開啟開發人員工具
    })
    this.page = await this.browser.newPage()
    await this.page.setViewport({width:1200,height:1000})
    await this.page.setDefaultTimeout(600000)//會修改goto,goBack,goForward,reload, setContent, waitForNavigation, page.waitForFunction, page.waitForFileChooser,page.waitForSelector等method的時間，預設是 30 秒
    await this.page.setDefaultNavigationTimeout(600000)//會修改goto,goBack,goForward,reload, setContent, waitForNavigation等method的時間，預設是 30 秒
})

After({timeout: 12 * 5000},async function () {
    await this.browser.close()
})

//Zowie - Mouse Bundgee

//en-us-catch all url
const zowieENUSurl=[]
const zowieENUSurlCollect = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://zowie.benq.com/en-us/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "http"
            if(url.indexOf(publishUrl)!==-1){
                zowieENUSurl.push(url)
            }
    })
    return zowieENUSurl;
    } catch (error) {
      console.log(error);
    }
};


Given("check enus Mouse Bungee",{timeout: 12000 * 5000},async function(){
    const pass=[]
    const fail=[]
    const zowieENUSurlReturnedData = await zowieENUSurlCollect();
    console.log(zowieENUSurlReturnedData)
    for(let i =0; i<zowieENUSurlReturnedData.length; i++){
        const zowieurlCheck = zowieENUSurlReturnedData[i]
        await this.page.goto("https://zowie.benq.com/en-us/index.html")
        await this.page.goto(zowieurlCheck)
        await this.page.waitForSelector('html')
        // const innerHtml = await page.$eval('body', element => element.innerHTML);
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);

        if(zowieurlCheck.indexOf("mouse-bungee")>0){
            console.log(`Fail URL: ${zowieurlCheck}`)
            fail.push(zowieurlCheck)
        }else{
            if(innerHtml.indexOf("Mouse Bungee")<0){
                console.log(`Pass URL: ${zowieurlCheck}`)
                pass.push(zowieurlCheck)
            }else{
                console.log(`Fail URL: ${zowieurlCheck}`)
                fail.push(zowieurlCheck)
            }
        }
    }
    console.log("enus pass url",pass)
    console.log("enus fail url",fail)
    if(fail.length>0){
        throw new Error(`Mouse Bungee有出現在以下這些URL: ${fail} `)
    }
})

//en-ca-catch all url
const zowieENCAurl=[]
const zowieENCAurlCollect = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://zowie.benq.com/en-ca/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "http"
            if(url.indexOf(publishUrl)!==-1){
                zowieENCAurl.push(url)
            }
    })
    return zowieENCAurl;
    } catch (error) {
      console.log(error);
    }
};

Given("check enca Mouse Bungee",{timeout: 12000 * 5000},async function(){
    const pass=[]
    const fail=[]
    const zowieENCAurlReturnedData = await zowieENCAurlCollect();
    console.log(zowieENCAurlReturnedData )
    for(let i =0; i<zowieENCAurlReturnedData .length; i++){
        const zowieurlCheck = zowieENCAurlReturnedData[i]
        await this.page.goto("https://zowie.benq.com/en-ca/index.html")
        await this.page.goto(zowieurlCheck)
        await this.page.waitForSelector('html')
        // const innerHtml = await page.$eval('body', element => element.innerHTML);
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // console.log(bqcUrlCheck,"灵感成真index",innerHtml.indexOf("灵感成真"))

        if(zowieurlCheck.indexOf("mouse-bungee")>0){
            console.log(`Fail URL: ${zowieurlCheck}`)
            fail.push(zowieurlCheck)
        }else{
            if(innerHtml.indexOf("Mouse Bungee")<0){
                console.log(`Pass URL: ${zowieurlCheck}`)
                pass.push(zowieurlCheck)
            }else{
                console.log(`Fail URL: ${zowieurlCheck}`)
                fail.push(zowieurlCheck)
            }
        }
    }
    console.log("enca pass url",pass)
    console.log("enca fail url",fail)
    if(fail.length>0){
        throw new Error(`Mouse Bungee有出現在以下這些URL: ${fail} `)

    }
})

//en-uk-catch all url
const zowieENUKurl=[]
const zowieENUKurlCollect = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://zowie.benq.eu/en-uk/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "http"
            if(url.indexOf(publishUrl)!==-1){
                zowieENUKurl.push(url)
            }
    })
    return zowieENUKurl;
    } catch (error) {
      console.log(error);
    }
};

Given("check enuk Mouse Bungee",{timeout: 12000 * 5000},async function(){
    const pass=[]
    const fail=[]
    const zowieENUKurlReturnedData = await zowieENUKurlCollect();
    console.log(zowieENUKurlReturnedData)
    for(let i =0; i<zowieENUKurlReturnedData .length; i++){
        const zowieurlCheck = zowieENUKurlReturnedData[i]
        await this.page.goto("https://zowie.benq.eu/en-uk/index.html")
        await this.page.goto(zowieurlCheck)
        await this.page.waitForSelector('html')
        // const innerHtml = await page.$eval('body', element => element.innerHTML);
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // console.log(bqcUrlCheck,"灵感成真index",innerHtml.indexOf("灵感成真"))
        if(zowieurlCheck.indexOf("mouse-bungee")>0){
            console.log(`Fail URL: ${zowieurlCheck}`)
            fail.push(zowieurlCheck)
        }else{
            if(innerHtml.indexOf("Mouse Bungee")<0){
                console.log(`Pass URL: ${zowieurlCheck}`)
                pass.push(zowieurlCheck)
            }else{
                console.log(`Fail URL: ${zowieurlCheck}`)
                fail.push(zowieurlCheck)
            }
        }
    }
    console.log("enuk pass url",pass)
    console.log("enuk fail url",fail)
    if(fail.length>0){
        throw new Error(`Mouse Bungee有出現在以下這些URL: ${fail} `)
    }
})

//en-eu-catch all url
const zowieENEUurl=[]
const zowieENEUurlCollect = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://zowie.benq.eu/en-eu/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "http"
            if(url.indexOf(publishUrl)!==-1){
                zowieENEUurl.push(url)
            }
    })
    return zowieENEUurl;
    } catch (error) {
      console.log(error);
    }
};

Given("check eneu Mouse Bungee",{timeout: 12000 * 5000},async function(){
    const pass=[]
    const fail=[]
    const zowieENEUurlReturnedData = await zowieENEUurlCollect();
    console.log(zowieENEUurlReturnedData)
    for(let i =0; i<zowieENEUurlReturnedData .length; i++){
        const zowieurlCheck = zowieENEUurlReturnedData[i]
        await this.page.goto("https://zowie.benq.eu/en-eu/index.html")
        await this.page.goto(zowieurlCheck)
        await this.page.waitForSelector('html')
        // const innerHtml = await page.$eval('body', element => element.innerHTML);
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if(zowieurlCheck.indexOf("mouse-bungee")>0){
            console.log(`Fail URL: ${zowieurlCheck}`)
            fail.push(zowieurlCheck)
        }else{
            if(innerHtml.indexOf("Mouse Bungee")<0){
                console.log(`Pass URL: ${zowieurlCheck}`)
                pass.push(zowieurlCheck)
            }else{
                console.log(`Fail URL: ${zowieurlCheck}`)
                fail.push(zowieurlCheck)
            }
        }
    }
    console.log("eneu pass url",pass)
    console.log("eneu fail url",fail)
    if(fail.length>0){
        throw new Error(`Mouse Bungee有出現在以下這些URL: ${fail} `)
    }
})

//en-pl-catch all url
const zowieENPLurl=[]
const zowieENPLurlCollect = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://zowie.benq.eu/en-pl/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "http"
            if(url.indexOf(publishUrl)!==-1){
                zowieENPLurl.push(url)
            }
    })
    return zowieENPLurl;
    } catch (error) {
      console.log(error);
    }
};

Given("check enpl Mouse Bungee",{timeout: 12000 * 5000},async function(){
    const pass=[]
    const fail=[]
    const zowieENPLurlReturnedData = await zowieENPLurlCollect();
    console.log(zowieENPLurlReturnedData)
    for(let i =0; i<zowieENPLurlReturnedData.length; i++){
        const zowieurlCheck = zowieENPLurlReturnedData[i]
        await this.page.goto("https://zowie.benq.eu/en-pl/index.html")
        await this.page.goto(zowieurlCheck)
        await this.page.waitForSelector('html')
        // const innerHtml = await page.$eval('body', element => element.innerHTML);
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if(zowieurlCheck.indexOf("mouse-bungee")>0){
            console.log(`Fail URL: ${zowieurlCheck}`)
            fail.push(zowieurlCheck)
        }else{
            if(innerHtml.indexOf("Mouse Bungee")<0){
                console.log(`Pass URL: ${zowieurlCheck}`)
                pass.push(zowieurlCheck)
            }else{
                console.log(`Fail URL: ${zowieurlCheck}`)
                fail.push(zowieurlCheck)
            }
        }
    }
    console.log("enplpass url",pass)
    console.log("enplfail url",fail)
    if(fail.length>0){
        throw new Error(`Mouse Bungee有出現在以下這些URL: ${fail} `)
    }
})

//en-fr-catch all url
const zowieENFRurl=[]
const zowieENFRurlCollect = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://zowie.benq.eu/en-fr/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "http"
            if(url.indexOf(publishUrl)!==-1){
                zowieENFRurl.push(url)
            }
    })
    return zowieENFRurl;
    } catch (error) {
      console.log(error);
    }
};

Given("check enfr Mouse Bungee",{timeout: 12000 * 5000},async function(){
    const pass=[]
    const fail=[]
    const zowieENFRurlReturnedData = await zowieENFRurlCollect();
    console.log(zowieENFRurlReturnedData)
    for(let i =0; i<zowieENFRurlReturnedData.length; i++){
        const zowieurlCheck = zowieENFRurlReturnedData[i]
        await this.page.goto("https://zowie.benq.eu/en-fr/index.html")
        await this.page.goto(zowieurlCheck)
        await this.page.waitForSelector('html')
        // const innerHtml = await page.$eval('body', element => element.innerHTML);
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if(zowieurlCheck.indexOf("mouse-bungee")>0){
            console.log(`Fail URL: ${zowieurlCheck}`)
            fail.push(zowieurlCheck)
        }else{
            if(innerHtml.indexOf("Mouse Bungee")<0){
                console.log(`Pass URL: ${zowieurlCheck}`)
                pass.push(zowieurlCheck)
            }else{
                console.log(`Fail URL: ${zowieurlCheck}`)
                fail.push(zowieurlCheck)
            }
        }
    }
    console.log("enfrpass url",pass)
    console.log("enfrfail url",fail)
    if(fail.length>0){
        throw new Error(`Mouse Bungee有出現在以下這些URL: ${fail} `)
    }
})

//en-nl-catch all url
const zowieENNLurl=[]
const zowieENNLurlCollect = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://zowie.benq.eu/en-nl/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "http"
            if(url.indexOf(publishUrl)!==-1){
                zowieENNLurl.push(url)
            }
    })
    return zowieENNLurl;
    } catch (error) {
      console.log(error);
    }
};

Given("check ennl Mouse Bungee",{timeout: 12000 * 5000},async function(){
    const pass=[]
    const fail=[]
    const zowieENNLurlReturnedData = await zowieENNLurlCollect();
    console.log(zowieENNLurlReturnedData)
    for(let i =0; i<zowieENNLurlReturnedData.length; i++){
        const zowieurlCheck = zowieENNLurlReturnedData[i]
        await this.page.goto("https://zowie.benq.eu/en-nl/index.html")
        await this.page.goto(zowieurlCheck)
        await this.page.waitForSelector('html')
        // const innerHtml = await page.$eval('body', element => element.innerHTML);
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if(zowieurlCheck.indexOf("mouse-bungee")>0){
            console.log(`Fail URL: ${zowieurlCheck}`)
            fail.push(zowieurlCheck)
        }else{
            if(innerHtml.indexOf("Mouse Bungee")<0){
                console.log(`Pass URL: ${zowieurlCheck}`)
                pass.push(zowieurlCheck)
            }else{
                console.log(`Fail URL: ${zowieurlCheck}`)
                fail.push(zowieurlCheck)
            }
        }
    }
    console.log("ennlpass url",pass)
    console.log("ennlfail url",fail)
    if(fail.length>0){
        throw new Error(`Mouse Bungee有出現在以下這些URL: ${fail} `)
    }
})

//en-ie-catch all url
const zowieENIEurl=[]
const zowieENIEurlCollect = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://zowie.benq.eu/en-ie/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "http"
            if(url.indexOf(publishUrl)!==-1){
                zowieENIEurl.push(url)
            }
    })
    return zowieENIEurl;
    } catch (error) {
      console.log(error);
    }
};

Given("check enie Mouse Bungee",{timeout: 12000 * 5000},async function(){
    const pass=[]
    const fail=[]
    const zowieENIEurlReturnedData = await zowieENIEurlCollect();
    console.log(zowieENIEurlReturnedData)
    for(let i =0; i<zowieENIEurlReturnedData.length; i++){
        const zowieurlCheck = zowieENIEurlReturnedData[i]
        await this.page.goto("https://zowie.benq.eu/en-ie/index.html")
        await this.page.goto(zowieurlCheck)
        await this.page.waitForSelector('html')
        // const innerHtml = await page.$eval('body', element => element.innerHTML);
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if(zowieurlCheck.indexOf("mouse-bungee")>0){
            console.log(`Fail URL: ${zowieurlCheck}`)
            fail.push(zowieurlCheck)
        }else{
            if(innerHtml.indexOf("Mouse Bungee")<0){
                console.log(`Pass URL: ${zowieurlCheck}`)
                pass.push(zowieurlCheck)
            }else{
                console.log(`Fail URL: ${zowieurlCheck}`)
                fail.push(zowieurlCheck)
            }
        }
    }
    console.log("eniepass url",pass)
    console.log("eniefail url",fail)
    if(fail.length>0){
        throw new Error(`Mouse Bungee有出現在以下這些URL: ${fail} `)
    }
})

//en-it-catch all url
const zowieENITurl=[]
const zowieENITurlCollect = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://zowie.benq.eu/en-it/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "http"
            if(url.indexOf(publishUrl)!==-1){
                zowieENITurl.push(url)
            }
    })
    return zowieENITurl;
    } catch (error) {
      console.log(error);
    }
};

Given("check enit Mouse Bungee",{timeout: 12000 * 5000},async function(){
    const pass=[]
    const fail=[]
    const zowieENITurlReturnedData = await zowieENITurlCollect();
    console.log(zowieENITurlReturnedData)
    for(let i =0; i<zowieENITurlReturnedData.length; i++){
        const zowieurlCheck = zowieENITurlReturnedData[i]
        await this.page.goto("https://zowie.benq.eu/en-it/index.html")
        await this.page.goto(zowieurlCheck)
        await this.page.waitForSelector('html')
        // const innerHtml = await page.$eval('body', element => element.innerHTML);
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if(zowieurlCheck.indexOf("mouse-bungee")>0){
            console.log(`Fail URL: ${zowieurlCheck}`)
            fail.push(zowieurlCheck)
        }else{
            if(innerHtml.indexOf("Mouse Bungee")<0){
                console.log(`Pass URL: ${zowieurlCheck}`)
                pass.push(zowieurlCheck)
            }else{
                console.log(`Fail URL: ${zowieurlCheck}`)
                fail.push(zowieurlCheck)
            }
        }
    }
    console.log("enitpass url",pass)
    console.log("enitfail url",fail)
    if(fail.length>0){
        throw new Error(`Mouse Bungee有出現在以下這些URL: ${fail} `)
    }
})

//en-se-catch all url
const zowieENSEurl=[]
const zowieENSEurlCollect = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://zowie.benq.eu/en-se/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "http"
            if(url.indexOf(publishUrl)!==-1){
                zowieENSEurl.push(url)
            }
    })
    return zowieENSEurl;
    } catch (error) {
      console.log(error);
    }
};

Given("check ense Mouse Bungee",{timeout: 12000 * 5000},async function(){
    const pass=[]
    const fail=[]
    const zowieENSEurlReturnedData = await zowieENSEurlCollect();
    console.log(zowieENSEurlReturnedData)
    for(let i =0; i<zowieENSEurlReturnedData.length; i++){
        const zowieurlCheck = zowieENSEurlReturnedData[i]
        await this.page.goto("https://zowie.benq.eu/en-se/index.html")
        await this.page.goto(zowieurlCheck)
        await this.page.waitForSelector('html')
        // const innerHtml = await page.$eval('body', element => element.innerHTML);
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if(zowieurlCheck.indexOf("mouse-bungee")>0){
            console.log(`Fail URL: ${zowieurlCheck}`)
            fail.push(zowieurlCheck)
        }else{
            if(innerHtml.indexOf("Mouse Bungee")<0){
                console.log(`Pass URL: ${zowieurlCheck}`)
                pass.push(zowieurlCheck)
            }else{
                console.log(`Fail URL: ${zowieurlCheck}`)
                fail.push(zowieurlCheck)
            }
        }
    }
    console.log("ensepass url",pass)
    console.log("ensefail url",fail)
    if(fail.length>0){
        throw new Error(`Mouse Bungee有出現在以下這些URL: ${fail} `)
    }
})